//
//  BookingView.swift
//  Plumbal
//
//  Created by Casperon iOS on 15/3/2017.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

protocol BookingViewDelegate {
    
    func pressedCancel(_ sender: BookingView)
    func pressBooking (_ confimDate: NSString, Confirmtime : NSString, Instructionstr: NSString)
    
}


class BookingView: UIViewController,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet var lblbookingDate: UILabel!
    @IBOutlet var lblBookingTime: UILabel!
    @IBOutlet var lblTaskerName: UILabel!
    @IBOutlet var titleHeader: UILabel!
    var taskernamestr : String = ""
    @IBOutlet var cancel: UIButton!
    @IBOutlet var confirm: UIButton!
    @IBOutlet var bottomview: SetColorView!
    @IBOutlet var instruction: UITextView!
    @IBOutlet var bookingdate: UILabel!
    @IBOutlet var bookingtime: UILabel!
    @IBOutlet var taskername: UILabel!
    var urlHandler = URLhandler()
    var extraFields : String =  String()
   
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var extraFieldsTable: UITableView!
    
    var TextViewPlaceHolder = ""
    var hourtime : String = String()
    var railywaytime :String = String()
    var delegate:BookingViewDelegate?
    var fieldsArray : NSMutableArray = NSMutableArray()

    @IBAction func confirmBooking(_ sender: AnyObject) {
        
        var fullString : String = ""
        
        for (index, val) in fieldsArray.enumerated() {
            
            print("value\(val as! String), Index : \(index)")
            
            let indexPath : NSIndexPath = NSIndexPath(row: index, section: 0)
            let cell : ExtraFieldsCell = self.extraFieldsTable.cellForRow(at: indexPath as IndexPath) as! ExtraFieldsCell
            
            guard let data : String = cell.extraField.text as! String else {
                continue
            }
            if fullString == "" {
                fullString = "\(val as! String) \(data)"
            } else {
                fullString += "\r\n \(val as! String) \(data)"
                //fullString += val as! String
            }
        }
       
       // print("Fields text : \(fullString)")
        
        //				fullString = "Sample text"
        
        self.delegate?.pressBooking(bookingdate.text! as NSString,Confirmtime: railywaytime as NSString,Instructionstr: fullString as NSString)
        /*if instruction.text! == TextViewPlaceHolder
        {
            themes.AlertView("\(Appname)", Message:"\(themes.setLang("enter_instruc"))", ButtonTitle: kOk)

        }
        else{
            self.delegate?.pressBooking(bookingdate.text! as NSString,Confirmtime: railywaytime as NSString,Instructionstr: fullString as NSString)
        }*/
    }
    
    @IBAction func cancelbooking(_ sender: AnyObject) {
        self.delegate?.pressedCancel(self)

    }
    override func viewWillAppear(_ animated: Bool) {
        
        print(extraFields)
        let fieldData : NSArray = extraFields.components(separatedBy: "\r\n" as String) as NSArray
        if fieldData.count == 0 {
            extraFieldsTable.isHidden = true
        } else {
            fieldsArray.addObjects(from: fieldData as! [Any])
            extraFieldsTable.isHidden = false
            extraFieldsTable.reload()
            
        }
        
        print("Fields \(fieldsArray)")
        
        self.view.layer.cornerRadius = 5
        view.layer.borderWidth=1.0
        view.layer.borderColor=themes.ThemeColour().cgColor
        self.view.layer.masksToBounds = true
        titleHeader.text = themes.setLang("job_confirmation")
lblTaskerName.text = themes.setLang("user_name")
        lblBookingTime.text = themes.setLang("book_time")
        lblbookingDate.text = themes.setLang("book_date")
        confirm.setTitle(themes.setLang("confirm_book"), for: UIControlState())
        cancel.setTitle(themes.setLang("cancel_book"), for: UIControlState())
confirm.titleLabel?.adjustsFontSizeToFitWidth = true
        
        TextViewPlaceHolder=themes.setLang("enter_instruc")

        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "MM/dd/yyyy"
        
        let result = formatter.string(from: date)
        
        formatter.dateFormat = "HH:mm"
        railywaytime  = formatter.string(from: date)
        
        formatter.dateFormat = "h:mm a"
        hourtime = formatter.string(from: date)
        
        bookingdate.text = result
        self.bookingtime.text = hourtime.lowercased()
        self.taskername.text = taskernamestr as String
        
        
        if( self.instruction.text == "") {
            self.instruction.text=TextViewPlaceHolder
        }
        instruction.delegate=self
        instruction.layer.borderWidth=1.0
        instruction.layer.cornerRadius = 5
        instruction.layer.borderColor=PlumberThemeColor.cgColor
        instruction.font=PlumberMediumFont
        
         bottomview.layer.cornerRadius = 5
        bottomview.layer.masksToBounds = true
        

        // Do any additional setup after loading the view.
        
        let screenHeight : CGFloat = CGFloat(280 + (fieldsArray.count * 40))
        let originalScreenHeight : CGFloat = UIScreen.main.bounds.size.height - 60
        
        if screenHeight > originalScreenHeight {
            self.view.frame = CGRect(x: 0, y: 0, width: 260, height: originalScreenHeight)
        } else {
            self.view.frame = CGRect(x: 0, y: 0, width: 260, height: screenHeight)
            self.scrollView.isScrollEnabled = true
        }
        
        self.extraFieldsTable.frame = CGRect(x: 0, y: 165, width: 260, height: fieldsArray.count * 40)
        self.extraFieldsTable.tableFooterView = UIView()
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: 180 + self.extraFieldsTable.frame.size.height)
        
        self.view.layoutIfNeeded()
        
        
    }
    
    override func viewDidLoad() {
        
        let Registernibextra=UINib(nibName: "ExtraFieldsCell", bundle: nil)
        extraFieldsTable.register(Registernibextra, forCellReuseIdentifier: "extraFieldCell")
        extraFieldsTable.separatorColor=UIColor.clear
        extraFieldsTable.isScrollEnabled  = false
        extraFieldsTable.dataSource = self
        extraFieldsTable.delegate = self
        //extraFieldsTable.isHidden =  true
    }
    
   
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
      
        if(instruction.text == TextViewPlaceHolder) {
            instruction.textColor=UIColor.black
            instruction.text=""
        }
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
     
        if(instruction.text == "") {
            instruction.textColor=PlumberThemeColor
            instruction.text=TextViewPlaceHolder
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let extraCell : ExtraFieldsCell = tableView.dequeueReusableCell(withIdentifier: "extraFieldCell") as! ExtraFieldsCell
        extraCell.selectionStyle = .none
        extraCell.titleLbl.isHidden = true
        
        let placeStr : String = (fieldsArray.object(at: indexPath.row) as? String)!
        placeStr.replacingOccurrences(of: ":", with: "", options: .literal, range: nil)
        
        extraCell.extraField.placeholder = placeStr
        extraCell.extraField.layer.borderColor = PlumberThemeColor.cgColor
        extraCell.extraField.layer.borderWidth = 1.0
        extraCell.extraField.layer.cornerRadius = 3
        extraCell.extraField.layer.masksToBounds = true
        extraCell.extraField.delegate = self
        extraCell.extraField.tag = indexPath.row
       // extraCell.contentView.backgroundColor = .red
        extraCell.extraField.frame = CGRect(x: 10, y: 5, width: 320 , height: 30)
        return extraCell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    // MARK:- TextField Delegate
    
   
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let textfield : UITextField = textField
        let tag : Int = textfield.tag
        print("Tag value : \(tag)")
        self.scrollView.setContentOffset(CGPoint(x: 0, y: (tag*40)+50), animated: true)
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        let textfield : UITextField = textField
        textfield.resignFirstResponder()
        return true
    }
    
    

}
