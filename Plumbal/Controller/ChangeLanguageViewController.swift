//
//  ChangeLanguageViewController.swift
//  Plumbal
//
//  Created by Casperon iOS on 10/10/2017.
//  Copyright © 2017 Casperon Tech. All rights reserved.
//

import UIKit

class ChangeLanguageViewController: UIViewController {
    
    @IBOutlet var Tamil: UIButton!
    
    @IBOutlet var English: UIButton!
    
    @IBOutlet var lang_change: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lang_change.text =  themes.setLang("lang_change")
        Tamil.setTitle("Tamil", for: UIControlState())
        English.setTitle("English", for: UIControlState())
        
        self.Tamil.layer.cornerRadius = 25
        self.Tamil.clipsToBounds = true
        self.English.layer.cornerRadius = 25
        self.English.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Tamil(_ sender: AnyObject) {
      
        themes.saveLanguage("ta")
        themes.SetLanguageToApp()
        SwapLanguage(language: themes.getAppLanguage())
    }
    
    
    @IBAction func English(_ sender: AnyObject)
    {
        
        
        themes.saveLanguage("en")
        themes.SetLanguageToApp()
        SwapLanguage(language: themes.getAppLanguage())
    }
    
    
    @IBAction func back(_ sender: AnyObject)
    {
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    
    
    
}
